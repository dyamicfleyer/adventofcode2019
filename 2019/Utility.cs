﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DyamicFleyer.AdventOfCode {
	public static class Utility {

		#region Configuration
		public static IDictionary<string, string> DeserializeConfig(string file) {
			using var reader = new StreamReader(File.Open(file, FileMode.OpenOrCreate, FileAccess.Read));

			var output = new Dictionary<string, string>();
			string line;
			while ((line = reader.ReadLine()) != null) {
				var pair = line.Split(new[] { '=' }, 2);
				if (pair.Length >= 2)
					output[pair[0]] = pair[1];
			}
			return output;
		}

		public static void SerializeConfig(string file, IDictionary<string, string> content) {
			using var writer = new StreamWriter(File.Open(file, FileMode.Create, FileAccess.Write));

			foreach (var pair in content) {
				writer.Write(pair.Key);
				writer.Write('=');
				writer.Write(pair.Value);
				writer.WriteLine();
			}
		}

		public static IDictionary<string, string> InitializeMissingConfigValues(string file, IDictionary<string, string> defaults) {
			var config = DeserializeConfig(file);
			foreach (var pair in defaults) {
				if (!config.ContainsKey(pair.Key))
					config.Add(pair);
			}
			SerializeConfig(file, config);
			return config;
		}
		#endregion

		#region Debug utilities
		public static string GetFriendlyName(this Type type) {
			var nullable = Nullable.GetUnderlyingType(type);
			if (nullable != null)
				return GetFriendlyName(nullable) + '?';

			var friendlyName = new StringBuilder(type.Name);
			if (type.IsGenericType) {
				int iBacktick = friendlyName.ToString().IndexOf('`');
				if (iBacktick > 0) {
					friendlyName = friendlyName.Remove(iBacktick, friendlyName.Length - iBacktick);
				}
				friendlyName.Append("<");
				Type[] typeParameters = type.GetGenericArguments();
				for (int i = 0; i < typeParameters.Length; ++i) {
					var typeParamName = GetFriendlyName(typeParameters[i]);
					friendlyName.Append(i == 0 ? typeParamName : ", " + typeParamName);
				}
				friendlyName.Append(">");
			}

			return friendlyName.ToString();
		}

		public static string ToFriendlyString<T>(this T obj) {
			if (obj is null)
				return $"({typeof(T).GetFriendlyName()}) null";
			if (obj is string)
				return $@"""{obj}""";
			if (obj is char)
				return $"'{obj}'";
			if (obj is bool)
				return obj.ToString();

			if (obj is IDictionary d) {
				var builder = new StringBuilder($"({obj.GetType().GetFriendlyName()}) {{\n");
				foreach (var k in d.Keys) {
					builder.Append(String.Join("\n",
						ToFriendlyString(k)
							.Split('\n')
							.Select(l => "  " + l)
						)
					).Append(": ")
					.Append(String.Join("\n",
						ToFriendlyString(d[k])
							.Split('\n')
							.Select(l => "  " + l)
						)[2..]
					).Append(",\n");
				}
				if (builder[builder.Length - 2] == ',')
					builder.Remove(builder.Length - 2, 1);
				return builder.Append('}').ToString();
			}
			if (obj is IEnumerable e) {
				var builder = new StringBuilder($"({obj.GetType().GetFriendlyName()}) [\n");
				foreach (var el in e) {
					builder.Append(String.Join("\n",
						ToFriendlyString(el)
							.Split('\n')
							.Select(l => "  " + l)
						)
					).Append(",\n");
				}
				if (builder[builder.Length - 2] == ',')
					builder.Remove(builder.Length - 2, 1);
				return builder.Append(']').ToString();
			}
			return $"({obj.GetType().GetFriendlyName()}) {obj?.ToString() ?? "null"}";
		}
		#endregion

		public static TValue Put<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, Func<TValue, TValue> func) {
			return dictionary[key] = func.Invoke(dictionary.GetValueOrDefault(key));
		}

		public static int RunIntcodeProgram(int[] intcode, TextReader inReader = null) {
			inReader ??= Console.In;

			var output = new List<int>();
			int lastOutput = -1;
			int ptr = 0;
			while (ptr < intcode.Length) {
				var opcode = intcode[ptr];
				var mode = (opcode / 100).ToString();
				switch (opcode % 100) {
					case 99: // terminate
						if (output.Count > 0 && AdventOfCode2019.Debug)
							Console.WriteLine($"  Output: {String.Join(',', output)}");
						return lastOutput;
					case 1: // add
						intcode[intcode[ptr + 3]]
							= GetByMode(ref intcode, mode, ptr, 1)
							+ GetByMode(ref intcode, mode, ptr, 2);
						ptr += 4;
						break;
					case 2: // multiply
						intcode[intcode[ptr + 3]]
							= GetByMode(ref intcode, mode, ptr, 1)
							* GetByMode(ref intcode, mode, ptr, 2);
						ptr += 4;
						break;
					case 3: // input
						if (inReader.Equals(Console.In))
							Console.Write($"  Instruction #{ptr} requires input: ");
						var line = inReader.ReadLine();
						if (line == null) {
							Console.Write($"  Instruction #{ptr} requires input, " +
								"and the specified reader couldn't provide one: ");
							line = Console.ReadLine();
						} else if (AdventOfCode2019.Debug) {
							Console.WriteLine($@"  Input: ""{line}""");
						}
						intcode[intcode[ptr + 1]] = Int32.Parse(line);
						ptr += 2;
						break;
					case 4: // output
						lastOutput = GetByMode(ref intcode, mode, ptr, 1);
						output.Add(lastOutput);
						ptr += 1;
						break;
					case 5: // jump if true
						if (GetByMode(ref intcode, mode, ptr, 1) != 0)
							ptr = GetByMode(ref intcode, mode, ptr, 2);
						else
							ptr += 3;
						break;
					case 6: // jump if false
						if (GetByMode(ref intcode, mode, ptr, 1) == 0)
							ptr = GetByMode(ref intcode, mode, ptr, 2);
						else
							ptr += 3;
						break;
					case 7: // less than
						intcode[intcode[ptr + 3]]
							= GetByMode(ref intcode, mode, ptr, 1)
							< GetByMode(ref intcode, mode, ptr, 2)
							? 1 : 0;
						ptr += 4;
						break;
					case 8: // equals
						intcode[intcode[ptr + 3]]
							= GetByMode(ref intcode, mode, ptr, 1)
							== GetByMode(ref intcode, mode, ptr, 2)
							? 1 : 0;
						ptr += 4;
						break;
					default:
						ptr++;
						break;
				}
			}
			return -1;

			static int GetByMode(ref int[] intcode, string mode, int ptr, int offset) {
				var intMode = mode.PadLeft(offset, '0')[^offset] - '0';

				return intMode switch
				{
					0 => intcode[intcode[ptr + offset]],
					1 => intcode[ptr + offset],
					_ => throw new ArgumentException($"{intMode} not recognized as a parameter mode.")
				};
			}
		}

		public static IEnumerable<T[]> GetPermutations<T>(T[] values, int startIndex = 0) {
			if (startIndex == values.Length - 1)
				yield return values;
			else {
				foreach (var v in GetPermutations(values, startIndex + 1))
					yield return v;

				for (var i = startIndex + 1; i < values.Length; i++) {
					SwapValues(values, startIndex, i);
					foreach (var v in GetPermutations(values, startIndex + 1))
						yield return v;
					SwapValues(values, startIndex, i);
				}
			}
		}

		private static void SwapValues<T>(T[] values, int index1, int index2) {
			if (index1 != index2) {
				T tmp = values[index1];
				values[index1] = values[index2];
				values[index2] = tmp;
			}
		}
	}
}
