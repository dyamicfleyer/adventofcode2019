﻿using DyamicFleyer.AdventOfCode.Solutions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;

namespace DyamicFleyer.AdventOfCode {
	public static class AdventOfCode2019 {

		private static readonly List<Type> Solutions = new List<Type> {
			typeof(Day01),
			typeof(Day02),
			typeof(Day03),
			typeof(Day04),
			typeof(Day05),
			typeof(Day06),
			typeof(Day07),
			typeof(Day08),
			typeof(Day09),
			typeof(Day11),
			typeof(Day12),
			typeof(Day13),
			typeof(Day14),
			typeof(Day15),
			typeof(Day16),
			typeof(Day17),
			typeof(Day18),
			typeof(Day19),
			typeof(Day21),
			typeof(Day22),
			typeof(Day23),
			typeof(Day24),
			typeof(Day25)
		};

		public static bool Debug { get; private set; }
		public static void Main(string[] args) {

			var config = Utility.InitializeMissingConfigValues(".config", new Dictionary<string, string> {
#if DEBUG
				{ "DEBUG", "true" },
#else
				{ "DEBUG", "false" },
#endif
				{ "SESSION", "(session_cookie)" }
			});
			Debug = args.Contains("--debug") || config["DEBUG"].Equals("true", StringComparison.OrdinalIgnoreCase);

			if (args.Length == 0 || !Byte.TryParse(args[0], out var day)) {
				if (DateTime.Today.Year == 2019 && DateTime.Today.Month == 12 && DateTime.Today.Day <= 25)
					day = (byte)DateTime.Today.Day;
				else
					throw new Exception("Cannot infer from the date which problem to solve. " +
						(DateTime.Today < new DateTime(2019, 12, 1)
							? "Try again in December!"
							: "Please specify a date as the first argument to this executable."));
			}
			if (day < 1 || day > 25)
				throw new Exception($"Specified date ({day}) is not a valid date between 1 and 25, inclusive.");

			Console.WriteLine($"Solving problem for December {day}...");

			string input;
			var dir = $"day{day:00}";
			Directory.CreateDirectory(dir);
			Directory.SetCurrentDirectory(dir);
			try {
				var inputFile = new FileInfo("input.txt");
				if (inputFile.Exists && inputFile.Length > 0) {
					input = File.ReadAllText(inputFile.Name);
				} else {
					using var client = new WebClient();
					client.Headers.Add(HttpRequestHeader.Cookie, $"session={config["SESSION"]}");
					try {
						input = client.DownloadString($"https://adventofcode.com/2019/day/{day}/input");
					} catch (WebException e) {
						Console.WriteLine("An error occured while attempting to download the input. Perhaps your session cookie is invalid, or this day's input has not yet been released?");
						Console.WriteLine(e);
						return;
					}
					File.WriteAllText(inputFile.Name, input);
				}
			} catch {
				Console.WriteLine($"Input for day {day} is not available.");
				return;
			}

			var stopwatch = new Stopwatch();

			using (var writer = new StreamWriter(File.Open("output.txt", FileMode.Create, FileAccess.Write))) {
				if (!(Activator.CreateInstance(Solutions[day - 1]) is Solution solution)) {
					Console.WriteLine($"some rando has added a type that doesn't extend {nameof(Solution)}... pretty messed up dude ngl");
					return;
				}
				string part1, part2;
				Console.WriteLine("Solving part 1...");
				try {
					stopwatch.Start();
					part1 = solution.Part1(input);
				} catch (NotImplementedException) {
					Console.WriteLine("  The solution for part 1 has not been implemented.");
					return;
				} catch (Exception e) {
					Console.WriteLine("An error occured while trying to solve part 1.");
					Console.WriteLine(e);
					return;
				} finally {
					stopwatch.Stop();
				}
				Console.WriteLine($"  [Answer: Part 1] {part1}");
				Console.WriteLine($"  [Solved in {stopwatch.Elapsed}]");
				writer.WriteLine($"Part 1: {part1}");

				Console.WriteLine();
				writer.WriteLine();

				Console.WriteLine("Solving part 2...");
				try {
					stopwatch.Restart();
					part2 = solution.Part2(input);
				} catch (NotImplementedException) {
					Console.WriteLine("  The solution for part 2 has not been implemented.");
					return;
				} catch (Exception e) {
					Console.WriteLine("An error occured while trying to solve part 2.");
					Console.WriteLine(e);
					return;
				} finally {
					stopwatch.Stop();
				}
				Console.WriteLine($"  [Answer: Part 2] {part2}");
				Console.WriteLine($"  [Solved in {stopwatch.Elapsed}]");
				writer.WriteLine($"Part 2: {part2}");
			}
		}
	}
}
