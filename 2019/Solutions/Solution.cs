﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DyamicFleyer.AdventOfCode.Solutions {
	public abstract class Solution {
		public int Day { get; }

		protected Solution(int day) {
			Day = day;
		}

		public abstract string Part1(string input);
		public abstract string Part2(string input);

		protected static IEnumerable<string> ReadLines(string input) {
			using var reader = new StringReader(input);
			string line;
			while ((line = reader.ReadLine()) != null)
				yield return line;
		}

		protected static void Debug(object content) {
			if (AdventOfCode2019.Debug)
				Console.WriteLine($"  [DEBUG] {String.Join('\n', content.ToFriendlyString().Split('\n').Select(l => "  " + l))[2..]}");
		}
	}
}
