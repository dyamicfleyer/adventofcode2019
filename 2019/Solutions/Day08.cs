﻿using MoreLinq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DyamicFleyer.AdventOfCode.Solutions {
	public class Day08 : Solution {
		public Day08() : base(8) { }

		private const int LAYER_WIDTH = 25;
		private const int LAYER_HEIGHT = 6;
		private const int LAYER_SIZE = LAYER_WIDTH * LAYER_HEIGHT;

		public override string Part1(string input) {
			var layers = new List<byte[]>();
			byte[] cur = null;
			for (int i = 0; i < input.Length; i++) {
				if (i % LAYER_SIZE == 0) {
					if (cur != null)
						layers.Add(cur);
					cur = new byte[LAYER_SIZE];
				}
				cur[i % cur.Length] = (byte)(input[i] - '0');
			}
			var minLayer = layers.MinBy(l => l.Count(b => b == 0)).First();
			return (minLayer.Count(b => b == 1) * minLayer.Count(b => b == 2)).ToString();
		}

		public override string Part2(string input) {
			var layers = new Stack<byte[]>();
			byte[] cur = null;
			for (int i = 0; i < input.Length; i++) {
				if (i % LAYER_SIZE == 0) {
					if (cur != null)
						layers.Push(cur);
					cur = new byte[LAYER_SIZE];
				}
				cur[i % cur.Length] = (byte)(input[i] - '0');
			}
			var image = new byte[LAYER_SIZE];
			while (layers.Count > 0) {
				var layer = layers.Pop();
				for (int i = 0; i < layer.Length; i++) {
					if (layer[i] != 2)
						image[i] = layer[i];
				}
			}
			using var writer = new StreamWriter(File.Open("image.txt", FileMode.Create, FileAccess.Write));
			for (int y = 0; y < LAYER_HEIGHT; y++) {
				for (int x = 0; x < LAYER_WIDTH; x++) {
					writer.Write(image[LAYER_WIDTH * y + x] == 1 ? '■' : ' ');
					writer.Write(' ');
				}
				writer.WriteLine();
			}
			return "Image has been stored inside image.txt";
		}
	}
}
