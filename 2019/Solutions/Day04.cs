﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DyamicFleyer.AdventOfCode.Solutions {
	public class Day04 : Solution {
		public Day04() : base(4) { }

		public override string Part1(string input) {
			uint count = 0;
			var split = input.Split('-');
			var min = UInt32.Parse(split[0]);
			var max = UInt32.Parse(split[1]);
			for (var i = min + 1; i < max; i++) {
				if (IsValid(i))
					count++;
			}
			return count.ToString();

			static bool IsValid(uint number) {
				var strnum = number.ToString();
				if (strnum.Length != 6)
					return false;

				if (!strnum.GroupBy(i => i).Any(c => c.Count() >= 2))
					return false;

				char max = (char)0;
				foreach (var c in strnum) { 
					if (c < max)
						return false;
					max = c;
				}
				return true;
			}
		}

		public override string Part2(string input) {
			uint count = 0;
			var split = input.Split('-');
			var min = UInt32.Parse(split[0]);
			var max = UInt32.Parse(split[1]);
			for (var i = min + 1; i < max; i++) {
				if (IsValid(i))
					count++;
			}
			return count.ToString();

			static bool IsValid(uint number) {
				var strnum = number.ToString();
				if (strnum.Length != 6)
					return false;

				if (!strnum.GroupBy(i => i).Any(c => c.Count() == 2))
					return false;

				char max = (char)0;
				foreach (var c in strnum) {
					if (c < max)
						return false;
					max = c;
				}
				return true;
			}
		}
	}
}
