﻿using System;
using System.Linq;

namespace DyamicFleyer.AdventOfCode.Solutions {
	public class Day01 : Solution {
		public Day01() : base(1) { }

		public override string Part1(string input) {
			uint total = 0;
			foreach (var line in ReadLines(input).Select(l => UInt32.Parse(l))) {
				total += line / 3 - 2;
			}
			return total.ToString();
		}

		public override string Part2(string input) {
			int total = 0;
			foreach (var line in ReadLines(input).Select(l => Int32.Parse(l))) {
				var cur = line / 3 - 2;
				total += cur;
				do {
					cur = Math.Max(0, cur / 3 - 2);
					total += cur;
				} while (cur > 0);
			}
			return total.ToString();
		}
	}
}
