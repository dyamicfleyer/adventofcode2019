﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace DyamicFleyer.AdventOfCode.Solutions {
	public class Day03 : Solution {
		public Day03() : base(3) { }

		public override string Part1(string input) {
			var wirePaths = input
				.Split('\n', StringSplitOptions.RemoveEmptyEntries)
				.Select(l => l.Split(','))
				.ToArray();

			var wires = new List<HashSet<Point>>();
			for (int i = 0; i < wirePaths.Length; i++) {
				var curSet = new HashSet<Point>();
				int x = 0,
					y = 0;
				foreach (var s in wirePaths[i]) {
					Func<Point> increment = s[0] switch
					{
						'U' => () => new Point(x, --y),
						'D' => () => new Point(x, ++y),
						'L' => () => new Point(--x, y),
						'R' => () => new Point(++x, y),
						_ => throw new Exception()
					};
					var multiplier = Int32.Parse(s[1..]);
					for (int j = 0; j < multiplier; j++) {
						curSet.Add(increment());
					}
				}
				wires.Add(curSet);
			}
			var intersections = wires.Aggregate((cur, prev) => cur.Intersect(prev).ToHashSet());
			return intersections.Min(p => Math.Abs(p.X) + Math.Abs(p.Y)).ToString();
		}

		public override string Part2(string input) {
			var wirePaths = input
				.Split('\n', StringSplitOptions.RemoveEmptyEntries)
				.Select(l => l.Split(','))
				.ToArray();

			var wires = new List<List<Point>>();
			for (int i = 0; i < wirePaths.Length; i++) {
				var curList = new List<Point>();
				int x = 0,
					y = 0;
				foreach (var s in wirePaths[i]) {
					Func<Point> increment = s[0] switch
					{
						'U' => () => new Point(x, --y),
						'D' => () => new Point(x, ++y),
						'L' => () => new Point(--x, y),
						'R' => () => new Point(++x, y),
						_ => throw new Exception()
					};
					var multiplier = Int32.Parse(s[1..]);
					for (int j = 0; j < multiplier; j++) {
						curList.Add(increment());
					}
				}
				wires.Add(curList);
			}
			var intersections = new Dictionary<Point, List<int>>();
			foreach (var d in wires) {
				var curDict = new Dictionary<Point, int>();
				for (int i = 0; i < d.Count; i++) {
					if (!curDict.ContainsKey(d[i]))
						curDict[d[i]] = i + 1;
				}
				foreach (var pair in curDict) {
					intersections.Put(pair.Key, val => {
						if (val is null)
							val = new List<int>();
						val.Add(pair.Value);
						return val;
					});
				}
			}
			return intersections.Where(p => p.Value.Count > 1).Min(p => p.Value.Sum()).ToString();
		}
	}
}
